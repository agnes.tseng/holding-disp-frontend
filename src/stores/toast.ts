import { Toast } from 'bootstrap'
import { defineStore } from 'pinia'

export const useToastStore = defineStore('toastStore', () => {
  const toastMessage = ref('')

  const showToast = (id: 'success' | 'error', message: string) => {
    toastMessage.value = message

    const toastId = document.querySelector(`.${id}`)

    if (toastId) {
      const toast = new Toast(toastId)
      toast.show()
    }
  }

  return { showToast, toastMessage }
})
