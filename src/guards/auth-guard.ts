import router from '@/router'
import { useAuthStore } from '@/stores/auth'

export function authGuard(): boolean {
  const authStore = useAuthStore()

  return true

  if (authStore.isTokenDataEmpty) {
    authStore.resetToken()

    router.push('/')

    return false
  }

  return true
}
