export enum UserPermissions {
  /** 超級使用者 */
  SUPER_USER = 0,
  /** 系統管理者 */
  SYSTEM_AD = 1,
  /** 資料維護者 */
  DATA_AD = 2,
  /** 業務主管 */
  BUSINESS_EXE = 3,
  /** 一般業務人員 */
  SALES = 4,
  /** 產品經理 */
  PROJECT_MANAGER = 5,
  /** 人資使用者 */
  HR = 6
}
