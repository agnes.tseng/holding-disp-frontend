/* eslint-env node */
require('@rushstack/eslint-patch/modern-module-resolution')

/**
 * eslint-plugin-import
 *
 * https://github.com/import-js/eslint-plugin-import
 */
module.exports = {
  extends: [
    'plugin:vue/vue3-recommended',
    '@vue/eslint-config-prettier',
    '@vue/eslint-config-typescript',
    'plugin:vue-scoped-css/vue3-recommended'
  ],
  rules: {
    // https://typescript-eslint.io/rules/array-type
    '@typescript-eslint/array-type': 'warn',

    // https://typescript-eslint.io/rules/consistent-type-imports
    '@typescript-eslint/consistent-type-imports': 'error',

    // https://typescript-eslint.io/rules/no-unused-vars
    '@typescript-eslint/no-unused-vars': 'warn',

    // https://eslint.org/docs/latest/rules/no-unused-vars
    'no-unused-vars': 'off',

    // https://eslint.vuejs.org/rules/attributes-order.html
    'vue/attributes-order': [
      'warn',
      {
        order: [
          'DEFINITION',
          'LIST_RENDERING',
          'CONDITIONALS',
          'RENDER_MODIFIERS',
          'GLOBAL',
          ['UNIQUE', 'SLOT'],
          'TWO_WAY_BINDING',
          'OTHER_DIRECTIVES',
          'ATTR_DYNAMIC',
          'ATTR_STATIC',
          'ATTR_SHORTHAND_BOOL',
          'EVENTS',
          'CONTENT'
        ],
        alphabetical: true
      }
    ],

    // https://eslint.vuejs.org/rules/block-lang.html
    'vue/block-lang': [
      'error',
      {
        script: {
          lang: 'ts'
        },
        style: {
          lang: 'scss'
        }
      }
    ],

    // https://eslint.vuejs.org/rules/camelcase.html
    'vue/camelcase': ['error', { properties: 'never' }],

    // https://eslint.vuejs.org/rules/component-definition-name-casing.html
    'vue/component-definition-name-casing': ['error' | 'kebab-case'],

    // https://eslint.vuejs.org/rules/component-name-in-template-casing.html
    'vue/component-name-in-template-casing': [
      'warn',
      'kebab-case',
      {
        registeredComponentsOnly: false,
        ignores: []
      }
    ],

    // https://eslint.vuejs.org/rules/component-tags-order.html
    'vue/component-tags-order': [
      'warn',
      {
        order: ['script', 'template', 'style']
      }
    ],

    // https://eslint.vuejs.org/rules/html-self-closing.html
    'vue/html-self-closing': [
      'warn',
      { html: { normal: 'never', void: 'always' } }
    ],

    // https://eslint.vuejs.org/rules/multi-word-component-names.html
    'vue/multi-word-component-names': [
      'error',
      {
        ignores: ['index']
      }
    ],

    // https://eslint.vuejs.org/rules/order-in-components.html
    'vue/order-in-components': 'error',

    // https://future-architect.github.io/eslint-plugin-vue-scoped-css/rules/enforce-style-type.html
    'vue-scoped-css/enforce-style-type': 'error',

    // https://future-architect.github.io/eslint-plugin-vue-scoped-css/rules/no-unused-selector.html
    'vue-scoped-css/no-unused-selector': 'off',

    // https://future-architect.github.io/eslint-plugin-vue-scoped-css/rules/require-v-deep-argument.html
    'vue-scoped-css/require-v-deep-argument': 'off',

    // https://eslint.vuejs.org/rules/eqeqeq.html
    'vue/eqeqeq': ['error', 'always', { null: 'ignore' }]
  },
  overrides: [
    /**
     * .vue 檔在 template 中使用 type/interface 時
     * 會造成 @typescript-eslint/no-unused-vars 驗證異常
     * 因此使用 no-unused-vars
     */
    {
      files: ['*.vue'],
      rules: {
        // https://typescript-eslint.io/rules/no-unused-vars
        '@typescript-eslint/no-unused-vars': 'off',
        // https://eslint.org/docs/latest/rules/no-unused-vars
        'no-unused-vars': 'warn'
      }
    }
  ],
  globals: { defineOptions: 'readonly' }
}
