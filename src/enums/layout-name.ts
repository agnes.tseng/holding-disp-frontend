/**
 * 佈局（Layout）名稱
 */
export enum LayoutName {
  BLANK_LAYOUT = 'blank-layout',
  MAIN_LAYOUT = 'main-layout'
}
