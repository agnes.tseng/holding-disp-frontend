const array = [
  {
    name: 'test1',
    code: 'test1',
    select: false
  },
  {
    name: 'test2',
    code: 'test2',
    select: true
  },
  {
    name: 'test3',
    code: 'test3',
    select: true
  },
  {
    name: 'test4',
    code: 'test4',
    select: false
  },
  {
    name: 'test5',
    code: 'test5',
    select: false
  },
  {
    name: 'test6',
    code: 'test6',
    select: true
  }
]

a()
function a() {
  const trueList = array.filter((item) => item.select)
  const falseList = array.filter((item) => !item.select)

  trueList.concat(falseList)
}
