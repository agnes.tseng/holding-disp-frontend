import axios from 'axios'
import { storeToRefs } from 'pinia'
import { useAuthStore } from '@/stores/auth'
import pinia from '@/stores/pinia-instance'
import { useToastStore } from '@/stores/toast'

// 如有需要 loading 畫面可開啟
// const { isLoading } = storeToRefs(useLoadingStore(piniaInstance))

const authStore = useAuthStore(pinia)
const { tokenData } = storeToRefs(authStore)

const toastStore = useToastStore(pinia)

const axiosInstance = axios.create({
  baseURL: import.meta.env.API_BASE_URL
})

// 如有需要 loading 畫面可開啟
// let loadingCount = 0
// const addLoading = () => {
//   isLoading.value = true
//   loadingCount++
// }

// const closeLoading = () => {
//   setTimeout(() => {
//     loadingCount--
//     if (loadingCount === 0) {
//       isLoading.value = false
//     }
//   })
// }

axiosInstance.interceptors.request.use(
  (config) => {
    config.headers['authorizationKey'] = `Bearer ${tokenData?.value}`

    return { ...config }
  },
  (error) => {
    return Promise.reject(error)
  }
)

axiosInstance.interceptors.response.use(
  (response) => {
    if (response.status !== 200) {
      toastStore.showToast('error', response.data.message)
    }

    return response
  },
  (error) => {
    toastStore.showToast('error', error.message)
    return Promise.reject(error)
  }
)

export default axiosInstance
