import { defineStore } from 'pinia'

export const useAuthStore = defineStore('authStore', () => {
  const tokenData = ref<string | null>(null)
  const isTokenDataEmpty = computed(() => !tokenData.value)

  /** 設定 token */
  const setTokenData = (data: string): void => {
    tokenData.value = data
  }

  /** 重設 token */
  const resetToken = (): void => {
    tokenData.value = null
  }

  return {
    tokenData,
    isTokenDataEmpty,
    setTokenData,
    resetToken
  }
})
