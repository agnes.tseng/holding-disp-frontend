import { fileURLToPath, URL } from 'node:url'

import Vue from '@vitejs/plugin-vue'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import DefineOptions from 'unplugin-vue-define-options/vite'
import VueMacros from 'unplugin-vue-macros/vite'
import { defineConfig } from 'vite'
import dynamicImport from 'vite-plugin-dynamic-import'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    VueMacros({
      plugins: {
        vue: Vue()
      }
    }),
    /**
     * unplugin-auto-import setting
     *
     * https://github.com/antfu/unplugin-auto-import
     */
    AutoImport({
      eslintrc: {
        enabled: true
      },
      imports: ['vue', 'vue-router'],
      include: [
        /\.[tj]sx?$/, // .ts, .tsx, .js, .jsx
        /\.vue$/,
        /\.vue\?vue/, // .vue
        /\.md$/ // .md
      ]
    }),
    /**
     * unplugin-vue-components setting
     *
     * https://github.com/antfu/unplugin-vue-components
     */
    Components({}),
    /**
     * unplugin-vue-define-options
     *
     * https://vue-macros.sxzz.moe/macros/define-options.html
     */
    DefineOptions(),
    /**
     * vite-plugin-dynamic-import
     *
     * https://github.com/vite-plugin/vite-plugin-dynamic-import
     */
    dynamicImport()
  ],
  envDir: './src/env',
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url)),
      '~': fileURLToPath(new URL('/node_modules', import.meta.url))
    }
  }
})
