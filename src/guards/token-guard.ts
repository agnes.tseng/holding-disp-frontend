import type { RouteLocationNormalized } from 'vue-router'
import type { LoginResponse } from '@/models/login'
import { useAuthStore } from '@/stores/auth'

export function tokenRedirectGuard(to: RouteLocationNormalized): void {
  const authStore = useAuthStore()
  const { token } = to.query as unknown as LoginResponse

  authStore.setTokenData(token)
}
