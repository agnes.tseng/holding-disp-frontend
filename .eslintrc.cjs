/* eslint-env node */
require('@rushstack/eslint-patch/modern-module-resolution')

module.exports = {
  root: true,
  extends: [
    /**
     * unplugin-auto-import eslint setting
     *
     * https://github.com/antfu/unplugin-auto-import#eslint
     */
    './.eslintrc-auto-import.json',
    'eslint:recommended',
    './eslint-extends/.eslintrc.import.cjs',
    './eslint-extends/.eslintrc.sonarjs.cjs',
    './eslint-extends/.eslintrc.unicorn.cjs',
    './eslint-extends/.eslintrc.vue.cjs'
  ],
  parserOptions: {
    ecmaVersion: 'latest'
  },
  rules: {
    // https://eslint.org/docs/latest/rules/camelcase
    camelcase: ['error', { properties: 'never' }],

    // https://eslint.org/docs/latest/rules/eqeqeq
    eqeqeq: ['error', 'always', { null: 'ignore' }],

    // https://eslint.org/docs/latest/rules/no-unmodified-loop-condition
    'no-unmodified-loop-condition': 'error',

    // https://eslint.org/docs/latest/rules/prefer-const
    'prefer-const': 'warn'
  }
}
