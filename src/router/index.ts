import { createRouter, createWebHistory } from 'vue-router'
import { LayoutName } from '@/enums/layout-name'
import {
  CommonRouterName,
  BasicInfoRouterName,
  CustomerMaintainRouterName,
  BusinessManagementRouterName,
  BusinessDataRouterName,
  BusinessReportRouterName,
  SystemManagementRouterName
} from '@/enums/router-name'
import { authGuard } from '@/guards/auth-guard'
import { tokenRedirectGuard } from '@/guards/token-guard'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    // login
    {
      path: '/component-view',
      beforeEnter: [tokenRedirectGuard],
      component: () => import('@/views/component-view/component-view.vue')
    },
    // login
    {
      path: '/',
      component: () => import('@/views/login/login-index.vue')
    },
    // home
    {
      path: `/${CommonRouterName.HOME}`,
      meta: {
        layout: LayoutName.MAIN_LAYOUT
      },
      children: [
        {
          path: '',
          component: () => import('@/views/home/index.vue'),
          name: CommonRouterName.HOME
        }
      ]
    },
    // 基本資料
    {
      path: `/${BasicInfoRouterName.BASIC}`,
      meta: {
        layout: LayoutName.MAIN_LAYOUT
      },
      beforeEnter: [authGuard],
      children: [
        {
          path: BasicInfoRouterName.BASIC_PRODUCT,
          component: () => import('@/views/basic-info/basic-product.vue'),
          name: BasicInfoRouterName.BASIC_PRODUCT
        },
        {
          path: BasicInfoRouterName.BASIC_DEPARTMENT,
          component: () => import('@/views/basic-info/basic-department.vue'),
          name: BasicInfoRouterName.BASIC_DEPARTMENT
        },
        {
          path: BasicInfoRouterName.BASIC_CUSTOMER_LEVEL,
          component: () =>
            import('@/views/basic-info/basic-customer-level.vue'),
          name: BasicInfoRouterName.BASIC_CUSTOMER_LEVEL
        },
        {
          path: BasicInfoRouterName.BASIC_CUSTOMER_CATEGORY,
          component: () =>
            import('@/views/basic-info/basic-customer-category.vue'),
          name: BasicInfoRouterName.BASIC_CUSTOMER_CATEGORY
        },
        {
          path: BasicInfoRouterName.BASIC_CUSTOMER_SYSTEM_CATEGORY,
          component: () =>
            import('@/views/basic-info/basic-customer-system.vue'),
          name: BasicInfoRouterName.BASIC_CUSTOMER_SYSTEM_CATEGORY
        },
        {
          path: BasicInfoRouterName.BASIC_HEALTH_INSURANCE_CATEGORY,
          component: () =>
            import('@/views/basic-info/basic-health-insurance.vue'),
          name: BasicInfoRouterName.BASIC_HEALTH_INSURANCE_CATEGORY
        },
        {
          path: BasicInfoRouterName.BASIC_CUSTOMER_TITLE,
          component: () =>
            import('@/views/basic-info/basic-customer-title.vue'),
          name: BasicInfoRouterName.BASIC_CUSTOMER_TITLE
        },
        {
          path: BasicInfoRouterName.BASIC_MAIN_JOB,
          component: () => import('@/views/basic-info/basic-main-job.vue'),
          name: BasicInfoRouterName.BASIC_MAIN_JOB
        }
      ]
    },
    // 客戶維護
    {
      path: `/${CustomerMaintainRouterName.CUSTOMER_MAINTAIN}`,
      meta: {
        layout: LayoutName.MAIN_LAYOUT
      },
      beforeEnter: [authGuard],
      children: [
        {
          path: CustomerMaintainRouterName.CUSTOMER_ARCHITECTURE,
          component: () =>
            import('@/views/customer-maintain/customer-architecture.vue'),
          name: CustomerMaintainRouterName.CUSTOMER_ARCHITECTURE
        }
      ]
    },
    // 業務管理
    {
      path: `/${BusinessManagementRouterName.BUSINESS_MANAGEMENT}`,
      meta: {
        layout: LayoutName.MAIN_LAYOUT
      },
      beforeEnter: [authGuard],
      children: [
        {
          path: BusinessManagementRouterName.BUSINESS_LEVEL,
          component: () =>
            import('@/views/business-management/business-level.vue'),
          name: BusinessManagementRouterName.BUSINESS_LEVEL
        },
        {
          path: BusinessManagementRouterName.BUSINESS_RESPONSIBILITY,
          component: () =>
            import('@/views/business-management/business-responsibility.vue'),
          name: BusinessManagementRouterName.BUSINESS_RESPONSIBILITY
        },
        {
          path: BusinessManagementRouterName.BUSINESS_PRODUCT_MANAGER,
          component: () =>
            import('@/views/business-management/business-product-manager.vue'),
          name: BusinessManagementRouterName.BUSINESS_PRODUCT_MANAGER
        }
      ]
    },
    // 業務活動資料登錄
    {
      path: `/${BusinessDataRouterName.BUSINESS_ACTIVITY_DATA}`,
      meta: {
        layout: LayoutName.MAIN_LAYOUT
      },
      beforeEnter: [authGuard],
      children: [
        {
          path: BusinessDataRouterName.BUSINESS_VISIT_DATA,
          component: () =>
            import('@/views/business-activity/business-visit-data.vue'),
          name: BusinessDataRouterName.BUSINESS_VISIT_DATA
        },
        {
          path: BusinessDataRouterName.BUSINESS_ATTENDANCE_DATA,
          component: () =>
            import('@/views/business-activity/business-attendance-data.vue'),
          name: BusinessDataRouterName.BUSINESS_ATTENDANCE_DATA
        }
      ]
    },
    // 統計分析報表
    {
      path: `/${BusinessReportRouterName.BUSINESS_REPORT}`,
      meta: {
        layout: LayoutName.MAIN_LAYOUT
      },
      beforeEnter: [authGuard],
      children: [
        {
          path: BusinessReportRouterName.BUSINESS_VISIT_DATA,
          component: () => import('@/views/report/report-visit.vue'),
          name: BusinessReportRouterName.BUSINESS_VISIT_DATA
        },
        {
          path: BusinessReportRouterName.BUSINESS_ATTENDANCE_DATA,
          component: () => import('@/views/report/report-attendance.vue'),
          name: BusinessReportRouterName.BUSINESS_ATTENDANCE_DATA
        }
      ]
    },
    // 系統管理
    {
      path: `/${SystemManagementRouterName.SYSTEM_MANAGEMENT}`,
      meta: {
        layout: LayoutName.MAIN_LAYOUT
      },
      beforeEnter: [authGuard],
      children: [
        {
          path: SystemManagementRouterName.SYSTEM_USER_MANAGEMENT,
          component: () => import('@/views/system/system-user.vue'),
          name: SystemManagementRouterName.SYSTEM_USER_MANAGEMENT
        },
        {
          path: SystemManagementRouterName.SYSTEM_BUSINESS_MANAGEMENT,
          component: () => import('@/views/system/system-business.vue'),
          name: SystemManagementRouterName.SYSTEM_BUSINESS_MANAGEMENT
        }
      ]
    },
    // 404 - 找不到頁面
    {
      path: '/:pathMatch(.*)*',
      name: CommonRouterName.PAGE_NOT_FOUND,
      component: () => import('@/views/page-not-found/index.vue'),
      meta: {
        layout: LayoutName.BLANK_LAYOUT
      }
    }
  ]
})

export default router
