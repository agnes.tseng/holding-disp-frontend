import { createApp } from 'vue'
import { useDynamicLayout, type UseDynamicLayoutOption } from '@/plugins'
import { piniaInstance } from '@/stores'
import App from './app.vue'
import router from './router'

// UI framework
import 'bootstrap'

// style
import './styles/main.scss'

// other
import VueDatePicker from '@vuepic/vue-datepicker'
import '@vuepic/vue-datepicker/dist/main.css'
import Multiselect from 'vue-multiselect'

createApp(App)
  .use(piniaInstance)
  .use(router)
  .use<UseDynamicLayoutOption>(useDynamicLayout, {
    router,
    layoutInFolder: true
  })
  .component('VueDatePicker', VueDatePicker)
  .component('Multiselect', Multiselect)
  .mount('#app')
