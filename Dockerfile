FROM nginx:alpine
COPY ./dist /usr/share/nginx/html/
COPY ./nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
# 使用設定檔案時，為了在容器中正常執行，需要保持 daemon off;
CMD [ "nginx", "-g", "daemon off;" ]
