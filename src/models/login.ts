import type { UserPermissions } from '@/enums/user-permission'

export interface LoginRequest {
  account: string
  password: string
}

export interface LoginResponse {
  userId: string
  token: string
  expiredAt: string
  role: UserPermissions
}
