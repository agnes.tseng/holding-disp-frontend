/// <reference types="vite/client" />

interface ImportMetaEnv {
  /** Api base url */
  readonly API_BASE_URL: string
}

interface ImportMeta {
  readonly env: ImportMetaEnv
}
