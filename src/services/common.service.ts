import type { LoginRequest, LoginResponse } from '@/models/login'
import { ApiFactory } from '@/utils/api/api-factory'

export class CommonService extends ApiFactory {
  constructor() {
    super('users')
  }

  /** login */
  login(data: LoginRequest): Promise<LoginResponse> {
    return super.post('login', data)
  }
}

export default new CommonService()
