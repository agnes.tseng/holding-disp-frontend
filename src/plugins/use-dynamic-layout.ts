import type { App, Plugin } from 'vue'
import type { Router } from 'vue-router'

/**
 *  動態註冊佈局組件 - 設定
 */
export interface UseDynamicLayoutOption {
  /**
   * Vue Router 實例
   */
  router: Router
  /**
   *  Layout 引入路徑解析
   * - true: '@/layouts/{layout名稱}/index.vue'
   * - false: '@/layouts/{layout名稱}.vue'
   */
  layoutInFolder: boolean
}

/**
 * 動態註冊佈局組件 (Layout)
 * 由 router.meta.layout 設定頁面所使用的 Layout，未設定時，使用父層路由所設定的 Layout
 *
 * @param app - Vue App 實例
 * @param options - 設定
 */
export const useDynamicLayout: Plugin = {
  install(app: App, options: UseDynamicLayoutOption) {
    const { router, layoutInFolder } = options

    router.beforeEach((to) => {
      // 頁面所使用的 Layout，未設定時，使用父層路由所設定的 Layout
      const { layout } = to.meta

      // Layout 未註冊時，動態註冊
      if (typeof layout === 'string' && !app.component(layout)) {
        app.component(
          layout,
          defineAsyncComponent(
            () =>
              import(
                layoutInFolder
                  ? `@/layouts/${layout}/index.vue`
                  : `@/layouts/${layout}.vue`
              )
          )
        )
      }

      return true
    })
  }
}
