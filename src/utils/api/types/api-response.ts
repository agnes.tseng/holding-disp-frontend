import type { AxiosResponse } from 'axios'

export interface ApiResponseModel<T = any> {
  status: number
  message: string
  success: boolean
  totalCount?: number
  data: T
}

export type ApiResponse = AxiosResponse<Omit<ApiResponseModel, 'data'>>
export type ApiDataResponse<T> = AxiosResponse<ApiResponseModel<T>>
