/* eslint-env node */
require('@rushstack/eslint-patch/modern-module-resolution')

/**
 * eslint-plugin-import
 *
 * https://github.com/import-js/eslint-plugin-import
 */
module.exports = {
  extends: ['plugin:import/recommended', 'plugin:import/typescript'],
  settings: {
    'import/resolver': {
      alias: {
        map: [['@', './src']],
        extensions: ['.js', '.jsx', '.ts', '.tsx']
      },
      // load <rootDir>/tsconfig.json to eslint
      typescript: {}
    }
  },
  rules: {
    // https://github.com/import-js/eslint-plugin-import
    'import/default': 'warn',
    'import/export': 'warn',
    'import/named': 'warn',
    'import/namespace': 'warn',
    'import/no-unresolved': ['error'],
    'import/order': [
      'warn',
      {
        alphabetize: {
          order: 'asc',
          caseInsensitive: true
        },
        groups: [
          'builtin',
          'external',
          'internal',
          'parent',
          'sibling',
          'index',
          'object'
        ]
      }
    ]
  }
}
