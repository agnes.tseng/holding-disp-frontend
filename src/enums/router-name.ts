/**
 * 共用 - 路由名稱
 */
export enum CommonRouterName {
  /** 登入頁 */
  LOGIN = 'login',
  /** 首頁 */
  HOME = 'home',
  /** 404 - 找不到頁面 */
  PAGE_NOT_FOUND = 'page-not-found'
}

/**
 * 基本資料 - 路由名稱
 */
export enum BasicInfoRouterName {
  /** 基本資料 */
  BASIC = 'basic',
  /** 基本資料 - 產品 */
  BASIC_PRODUCT = 'product',
  /** 基本資料 - 科別 */
  BASIC_DEPARTMENT = 'department',
  /** 基本資料 - 客戶等級 */
  BASIC_CUSTOMER_LEVEL = 'rank',
  /** 基本資料 - 客戶類別 */
  BASIC_CUSTOMER_CATEGORY = 'category',
  /** 基本資料 - 客戶系統別 */
  BASIC_CUSTOMER_SYSTEM_CATEGORY = 'system-category',
  /** 基本資料 - 健保特約類別 */
  BASIC_HEALTH_INSURANCE_CATEGORY = 'health-insurance-category',
  /** 基本資料 - 客戶職稱 */
  BASIC_CUSTOMER_TITLE = 'title',
  /** 基本資料 - 主要工作 */
  BASIC_MAIN_JOB = 'main-job'
}

/**
 * 客戶維護 - 路由名稱
 */
export enum CustomerMaintainRouterName {
  /** 客戶維護 */
  CUSTOMER_MAINTAIN = 'customer-maintain',
  /** 客戶維護 - 客戶架構 */
  CUSTOMER_ARCHITECTURE = 'architecture'
}

/**
 * 業務管理 - 路由名稱
 */
export enum BusinessManagementRouterName {
  /** 業務管理 */
  BUSINESS_MANAGEMENT = 'business-management',
  /** 業務管理 - 業務層級 */
  BUSINESS_LEVEL = 'level',
  /** 業務管理 - 業務擔當 */
  BUSINESS_RESPONSIBILITY = 'responsibility',
  /** 業務管理 - 產品經理擔當 */
  BUSINESS_PRODUCT_MANAGER = 'product-manager'
}

/**
 * 業務活動資料登錄 - 路由名稱
 */
export enum BusinessDataRouterName {
  /** 業務活動資料登錄 */
  BUSINESS_ACTIVITY_DATA = 'business-activity',
  /** 業務活動資料登錄 - 業務拜訪紀錄 */
  BUSINESS_VISIT_DATA = 'visit-record',
  /** 業務活動資料登錄 - 業務出勤紀錄 */
  BUSINESS_ATTENDANCE_DATA = 'attendance-record'
}

/**
 * 統計分析報表 - 路由名稱
 */
export enum BusinessReportRouterName {
  /** 統計分析報表 */
  BUSINESS_REPORT = 'business-report',
  /** 統計分析報表 - 業務拜訪報表 */
  BUSINESS_VISIT_DATA = 'visit',
  /** 統計分析報表 - 業務出勤報表 */
  BUSINESS_ATTENDANCE_DATA = 'attendance'
}

/**
 * 系統管理 - 路由名稱
 */
export enum SystemManagementRouterName {
  /** 系統管理 */
  SYSTEM_MANAGEMENT = 'system-management',
  /** 系統管理 - 系統使用者管理 */
  SYSTEM_USER_MANAGEMENT = 'user',
  /** 系統管理 - 業務擔當導入 */
  SYSTEM_BUSINESS_MANAGEMENT = 'business'
}
