import type { AxiosRequestConfig } from 'axios'
import axiosInstance from './axios-instance'
import type { ApiDataResponse, ApiResponse } from './types/api-response'

export class ApiFactory {
  constructor(private apiUrl: string) {}

  /** 取得單筆資料 */
  protected getData(id: string): Promise<ApiDataResponse<any>> {
    return axiosInstance.get(`${this.apiUrl}/${id}`)
  }

  /** 取得多筆資料 */
  protected getDataList(id: string): Promise<ApiDataResponse<any>> {
    return axiosInstance.get(`${this.apiUrl}/${id}`)
  }

  /** 新增單筆資料 */
  protected createData(data: any): Promise<ApiResponse> {
    return axiosInstance.post(`${this.apiUrl}`, data)
  }

  /** 更新資料 */
  protected updateData(data: any): Promise<ApiResponse> {
    return axiosInstance.put(`${this.apiUrl}`, data)
  }

  /** 刪除資料 */
  protected deleteData(id: string): Promise<ApiResponse> {
    return axiosInstance.delete(`${this.apiUrl}/${id}`)
  }

  /** get 方法 */
  protected get<Param, Response = ApiResponse>(
    url: string,
    params?: Param,
    config?: AxiosRequestConfig
  ): Promise<Response> {
    const apiConfig: AxiosRequestConfig = {
      ...config,
      params: { ...config?.params, params }
    }

    return axiosInstance.get(`${this.apiUrl}/${url}`, apiConfig)
  }

  /** post 方法 */
  protected post<Data, Response = ApiResponse>(
    url: string,
    data: Data
  ): Promise<Response> {
    return axiosInstance.post(`${this.apiUrl}/${url}`, data)
  }

  /** put 方法 */
  protected put<Data, Response = ApiResponse>(
    url: string,
    data: Data
  ): Promise<Response> {
    return axiosInstance.put(`${this.apiUrl}/${url}`, data)
  }
}
